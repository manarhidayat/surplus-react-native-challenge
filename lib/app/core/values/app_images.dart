class Images {
  Images._();

  static const String logo = 'images/logo.png';
  static const String launcher = 'images/ic_launcher.png';
  static const String banner = 'images/banner-ad-examples.png';
  static const String certified_logo = 'images/certified_logo.png';
  static const String onboard = 'images/onboard.png';
}
