part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const MAIN = _Paths.MAIN;
  static const HOME = _Paths.HOME;
  static const ORDER = _Paths.ORDER;
  static const PROFILE = _Paths.PROFILE;
  static const OTHER = _Paths.OTHER;
  static const PROJECT_DETAILS = _Paths.PROJECT_DETAILS;
  static const SIGNIN = _Paths.SIGNIN;
  static const SIGNUP = _Paths.SIGNUP;
  static const ONBOARD = _Paths.ONBOARD;
  static const SPLASH = _Paths.SPLASH;
}

abstract class _Paths {
  static const MAIN = '/main';
  static const HOME = '/home';
  static const ORDER = '/order';
  static const PROFILE = '/profile';
  static const ONBOARD = '/onboard';
  static const SIGNIN = '/signin';
  static const SIGNUP = '/signup';
  static const OTHER = '/other';
  static const SPLASH = '/splash';
  static const PROJECT_DETAILS = '/project-details';
}
