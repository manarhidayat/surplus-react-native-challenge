// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:surplus_challenge/app/core/base/base_view.dart';
import 'package:surplus_challenge/app/core/values/app_colors.dart';
import 'package:surplus_challenge/app/core/values/app_dimens.dart';
import 'package:surplus_challenge/app/core/values/app_images.dart';
import 'package:surplus_challenge/app/core/values/app_values.dart';
import 'package:surplus_challenge/app/core/values/text_styles.dart';
import 'package:surplus_challenge/app/modules/signin/controllers/signin_controller.dart';
import 'package:surplus_challenge/app/routes/app_pages.dart';

class OnBoardView extends BaseView<SigninController> {
  @override
  PreferredSizeWidget? appBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size(0.0, 0.0),
      child: Container(),
    );
  }

  @override
  Widget body(BuildContext context) {
    return GetBuilder<SigninController>(builder: (controller) {
      return Container(
        color: AppColors.colorWhite,
        height: double.infinity,
        child: Stack(children: <Widget>[
          Image.asset(
            Images.onboard,
            width: double.infinity,
          ),
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: Container(
              padding: const EdgeInsets.all(16),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                    top: Radius.circular(30), bottom: Radius.circular(0)),
                color: Colors.white,
              ),
              child: Column(
                children: [
                  Text("Selamat datang di Surplus",
                      style: TextStyle(
                        fontSize: AppValues.textTitle,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      )),
                  AppDimens.verticalSpace12,
                  Text(
                      "Selamatkan makanan berlebih di aplikasi Surplus agar tidak terbuang sia-sia",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black45,
                      )),
                  AppDimens.verticalSpace40,
                  SizedBox(
                    width: double.infinity,
                    height: AppValues.heightButton,
                    child: OutlinedButton(
                      style: ButtonStyle(
                        padding: MaterialStateProperty.resolveWith(
                            (states) => const EdgeInsets.all(8.0)),
                        backgroundColor: MaterialStateProperty.resolveWith(
                            (states) => AppColors.colorPrimary),
                        shape: MaterialStateProperty.all(RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(AppValues.borderRadius))),
                      ),
                      onPressed: () {
                        Get.toNamed(Routes.SIGNUP);
                      },
                      child: Text(
                        'Daftar',
                        style: textButtonWhite,
                      ),
                    ),
                  ),
                  AppDimens.verticalSpace20,
                  SizedBox(
                    width: double.infinity,
                    height: AppValues.heightButton,
                    child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          backgroundColor: AppColors.textFieldColor,
                          side: BorderSide(color: AppColors.colorPrimary),
                          shape: const RoundedRectangleBorder(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(AppValues.borderRadius))),
                        ),
                        onPressed: () async {
                          Get.toNamed(Routes.SIGNIN);
                        },
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              AppDimens.horizontalSpace4,
                              Text(
                                'Sudah punya akun? Masuk',
                                style: TextStyle(
                                    color: AppColors.colorPrimary,
                                    fontSize: AppValues.fontSize_14,
                                    fontWeight: FontWeight.w500),
                              )
                            ])),
                  ),
                  AppDimens.verticalSpace20,
                  Container(
                    padding: EdgeInsets.all(8.0),
                    child: RichText(
                      text: TextSpan(
                        style: TextStyle(
                          color: AppColors.textColorPrimary,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                              text: 'Dengan daftar atau masuk, Anda menerima'),
                          TextSpan(
                              text: ' syarat dan ketentuan ',
                              style: TextStyle(color: AppColors.colorPrimary)),
                          TextSpan(text: 'serta '),
                          TextSpan(
                              text: 'Kebijakan privasi',
                              style: TextStyle(color: AppColors.colorPrimary)),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ]),
      );
    });
  }
}
