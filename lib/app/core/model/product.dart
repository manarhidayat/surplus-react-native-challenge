class Product {
  final String image;
  final String name;
  final String price;
  final String discount;
  final String priceBeforeDiscount;
  final String distance;
  final String status;
  final String time;
  final String rating;
  final String sold;
  bool isNew = false;

  Product(
      {required this.image,
      required this.name,
      required this.price,
      required this.discount,
      required this.priceBeforeDiscount,
      required this.distance,
      required this.status,
      required this.time,
      required this.rating,
      required this.sold,
      this.isNew = false});
}
