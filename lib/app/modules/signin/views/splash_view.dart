// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:surplus_challenge/app/core/values/app_colors.dart';
import 'package:surplus_challenge/app/core/values/app_dimens.dart';
import 'package:surplus_challenge/app/core/values/app_images.dart';
import 'package:surplus_challenge/app/routes/app_pages.dart';

class SplashView extends StatefulWidget {
  @override
  State<SplashView> createState() => _SplashViewState();
}

class _SplashViewState extends State<SplashView> {
  @override
  void initState() {
    super.initState();

    Future.delayed(const Duration(seconds: 3), () {
      Get.toNamed(Routes.ONBOARD);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: AppColors.colorWhite,
        height: double.infinity,
        child: Column(
          children: [
            Spacer(),
            Image.asset(
              Images.logo,
              width: 150,
            ),
            AppDimens.verticalSpace8,
            Text(
              "Save food, Save budget",
              style: TextStyle(color: AppColors.colorPrimary, fontSize: 16),
            ),
            Text(
              "Save planet",
              style: TextStyle(color: AppColors.colorPrimary, fontSize: 16),
            ),
            Spacer(),
            Image.asset(
              Images.certified_logo,
              width: 150,
            ),
            AppDimens.verticalSpace20,
          ],
        ));
  }
}
