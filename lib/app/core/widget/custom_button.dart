import 'package:flutter/material.dart';
import 'package:surplus_challenge/app/core/values/app_colors.dart';
import 'package:surplus_challenge/app/core/values/app_dimens.dart';
import 'package:surplus_challenge/app/core/values/app_values.dart';
import 'package:surplus_challenge/app/core/values/text_styles.dart';

class CustomButton extends StatelessWidget {
  final GestureTapCallback onPressed;
  final String text;
  final double? height;
  final double? width;
  final TextStyle textStyle;
  final Color color;
  final Color borderColor;
  final String? icon;

  const CustomButton(
      {Key? key,
      required this.onPressed,
      required this.text,
      this.height = AppValues.heightButton,
      this.width = double.infinity,
      this.textStyle = textButton,
      this.color = AppColors.colorPrimary,
      this.borderColor = AppColors.colorPrimary,
      this.icon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width ?? double.infinity,
      height: height,
      child: OutlinedButton(
        style: OutlinedButton.styleFrom(
          side: BorderSide(width: 1.0, color: borderColor),
          backgroundColor: color,
          // padding: MaterialStateProperty.resolveWith(
          //     (states) => const EdgeInsets.all(8.0)),
          // backgroundColor: MaterialStateProperty.resolveWith((states) => color),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(AppValues.borderRadius),
          ),
        ),
        onPressed: () {
          onPressed();
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            icon != null
                ? Image.asset(
                    icon!,
                    width: AppValues.iconSize_14,
                    color: AppColors.colorWhite,
                  )
                : Container(),
            icon != null ? AppDimens.horizontalSpace8 : Container(),
            Text(
              text,
              style: textStyle,
            ),
          ],
        ),
      ),
    );
  }
}
