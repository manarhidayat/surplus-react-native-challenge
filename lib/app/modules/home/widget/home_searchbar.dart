import 'package:flutter/material.dart';
import 'package:surplus_challenge/app/core/values/app_colors.dart';

class HomeSearchbar extends StatelessWidget {
  const HomeSearchbar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        decoration: InputDecoration(
          hintText: 'Mau selamatkan makanan apa hari ini?',
          fillColor: AppColors.colorWhite,
          filled: true,
          contentPadding:
              const EdgeInsets.symmetric(vertical: 15.0, horizontal: 10.0),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: AppColors.lightGreyColor),
            borderRadius: BorderRadius.circular(20.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: AppColors.lightGreyColor),
            borderRadius: BorderRadius.circular(20.0),
          ),
          prefixIcon: const Icon(
            Icons.search,
            color: AppColors.colorPrimary,
          ),
        ),
      ),
    );
  }
}
