// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:surplus_challenge/app/core/values/app_dimens.dart';
import 'package:surplus_challenge/app/modules/home/widget/home_banner.dart';
import 'package:surplus_challenge/app/modules/home/widget/home_header.dart';
import 'package:surplus_challenge/app/modules/home/widget/home_items.dart';
import 'package:surplus_challenge/app/modules/home/widget/home_searchbar.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: _title,
      home: HomeViewWidget(),
    );
  }
}

class HomeViewWidget extends StatefulWidget {
  const HomeViewWidget({Key? key}) : super(key: key);

  @override
  State<HomeViewWidget> createState() => HomeViewWidgetState();
}

class HomeViewWidgetState extends State<HomeViewWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
          floatHeaderSlivers: true,
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverOverlapAbsorber(
                handle:
                    NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                sliver: SliverAppBar(
                  pinned: true,
                  backgroundColor: Colors.white,
                  expandedHeight: 160.0,
                  flexibleSpace: HomeHeader(),
                  forceElevated: innerBoxIsScrolled,
                  bottom: PreferredSize(
                      preferredSize: const Size.fromHeight(10.0),
                      child: HomeSearchbar()),
                ),
              ),
            ];
          },
          body: SingleChildScrollView(
              child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AppDimens.verticalSpace60,
              AppDimens.verticalSpace60,
              HomeBanner(),
              Container(
                padding: const EdgeInsets.all(10.0),
                child: Text('Paling disukai',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                    )),
              ),
              SizedBox(
                height: 250.0,
                child: HomeItems(),
              ),
              Container(
                padding: const EdgeInsets.all(10.0),
                child: Text('Kami melewatkan ini',
                    style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 16,
                    )),
              ),
              SizedBox(
                height: 250.0,
                child: HomeItems(),
              ),
            ],
          ))),
    );
  }
}
