import 'package:flutter/material.dart';
import 'package:surplus_challenge/app/core/values/app_dimens.dart';
import '/app/core/base/base_widget_mixin.dart';
import '/app/core/values/app_values.dart';
import '/app/core/values/text_styles.dart';
import '/app/core/widget/ripple.dart';

class ItemProfile extends StatelessWidget with BaseWidgetMixin {
  final IconData image;
  final String description;
  final String title;
  final Function()? onTap;

  ItemProfile({
    required this.image,
    required this.title,
    required this.onTap,
    required this.description,
  });

  @override
  Widget body(BuildContext context) {
    return Ripple(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(AppValues.padding),
        child: Row(
          children: [
            Icon(image),
            const SizedBox(width: AppValues.smallPadding),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(title, style: settingsItemStyle),
                  AppDimens.verticalSpace4,
                  Text(description, style: descriptionTextStyle),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
