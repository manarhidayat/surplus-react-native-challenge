import 'package:flutter/material.dart';
import 'package:surplus_challenge/app/core/values/app_colors.dart';
import 'package:surplus_challenge/app/core/values/app_dimens.dart';
import 'package:surplus_challenge/app/core/values/app_values.dart';
import 'package:surplus_challenge/app/core/values/text_styles.dart';
import 'package:surplus_challenge/app/core/widget/custom_button.dart';
import '/app/modules/profile/widgets/item_profile_widgets.dart';
import '/app/core/base/base_view.dart';
import '/app/core/widget/custom_app_bar.dart';
import '/app/modules/profile/controllers/profile_controller.dart';

class ProfileView extends BaseView<ProfileController> {
  @override
  PreferredSizeWidget? appBar(BuildContext context) {
    return CustomAppBar(
      appBarTitleText: appLocalization.bottomNavProfile,
      isBackButtonEnabled: false,
    );
  }

  @override
  Widget body(BuildContext context) {
    return SingleChildScrollView(
      physics: const BouncingScrollPhysics(),
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(AppValues.padding),
            child: Column(
              children: [
                const Icon(
                  Icons.person_outline_rounded,
                  size: 70,
                ),
                AppDimens.verticalSpace8,
                const Text(
                  "Manar Hidayat",
                  style: titleStyle,
                ),
                AppDimens.verticalSpace4,
                const Text(
                  "manarhidayat@gmail.com",
                  style: descriptionTextStyle,
                ),
                AppDimens.verticalSpace4,
                const Text(
                  "0813227487868",
                  style: descriptionTextStyle,
                ),
                AppDimens.verticalSpace16,
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(AppValues.halfPadding),
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          border: Border.all(color: AppColors.colorPrimary)),
                      child: Column(
                        children: const [
                          Text('Kamu menyelmatkan'),
                          Text(
                            '0 Makanan',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    AppDimens.horizontalSpace18,
                    Container(
                      padding: const EdgeInsets.all(AppValues.halfPadding),
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(10)),
                          border: Border.all(color: AppColors.colorPrimary)),
                      child: Column(
                        children: const [
                          Text('Total Penghematan'),
                          Text(
                            'Rp 0',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          _getHorizontalDivider(),
          Container(
            padding: const EdgeInsets.all(AppValues.padding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  "Kode Referal",
                  style: titleStyle,
                ),
                AppDimens.verticalSpace4,
                const Text(
                  "Yuk bagikan kode referral ke keluarga serta temanmu! Dan dapatkan voucher makan sampai dengan 150 ribu loh!",
                  style: descriptionTextStyle,
                ),
                AppDimens.verticalSpace16,
                Container(
                  color: AppColors.colorSecondary,
                  padding: const EdgeInsets.symmetric(
                      vertical: AppValues.halfPadding,
                      horizontal: AppValues.padding),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      Text("m a n 2 7 6"),
                      Text("salin kode"),
                    ],
                  ),
                ),
                AppDimens.verticalSpace16,
                CustomButton(
                    onPressed: () {
                      print("bagikan kode");
                    },
                    text: " Bagikan Kode"),
              ],
            ),
          ),
          _getHorizontalDivider(),
          ItemProfile(
            title: "Voucher Saya",
            image: Icons.fastfood,
            description: 'Lihat Voucher kamu yang sedang aktif saat ini',
            onTap: _onItemClicked,
          ),
          _getHorizontalDivider(),
          ItemProfile(
            title: "Ulasan Makanan",
            image: Icons.fastfood,
            description:
                'Berikan ulasan kepada penjual makanan yang sudah kamu beli',
            onTap: _onItemClicked,
          ),
          _getHorizontalDivider(),
          ItemProfile(
            title: "Dampak Infografik",
            image: Icons.fastfood,
            description:
                'Cari tahu seberapa besar makanan yang telah kamu selamatkan lewat aplikasi',
            onTap: _onItemClicked,
          ),
          _getHorizontalDivider(),
        ],
      ),
    );
  }

  Widget _getHorizontalDivider() {
    return const Divider(height: 1);
  }

  void _onItemClicked() {
    showToast('Development in progress');
  }
}
