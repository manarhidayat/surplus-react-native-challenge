// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, avoid_unnecessary_containers
import 'package:flutter/material.dart';
import 'package:surplus_challenge/app/core/model/product.dart';
import 'package:surplus_challenge/app/core/values/app_colors.dart';
import 'package:surplus_challenge/app/core/values/app_dimens.dart';
import 'package:surplus_challenge/app/core/values/text_styles.dart';

class ItemProduct extends StatefulWidget {
  final Product product;
  const ItemProduct({Key? key, required this.product}) : super(key: key);

  @override
  State<StatefulWidget> createState() => ItemProductState();
}

class ItemProductState extends State<ItemProduct> {
  @override
  Widget build(BuildContext context) {
    Product product = widget.product;

    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: AppColors.lightGreyColor,
                  borderRadius: const BorderRadius.all(Radius.circular(5))),
              child: Stack(children: [
                Image.network(product.image, fit: BoxFit.contain, width: 180.0),
                Positioned(
                  top: 8.0,
                  right: 8.0,
                  child: Icon(
                    Icons.favorite_outline,
                    color: AppColors.lightGreyColor,
                  ),
                ),
                if (product.discount != "")
                  Positioned(
                      bottom: 8.0,
                      left: 0,
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 4.0, horizontal: 8.0),
                        // color: Colors.red,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(8),
                            bottomRight: Radius.circular(8),
                          ),
                        ),
                        child: Text("${product.distance}",
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 14)),
                      ))
                else if (product.isNew)
                  Positioned(
                      bottom: 8.0,
                      left: 0,
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 4.0, horizontal: 8.0),
                        // color: Colors.red,
                        decoration: BoxDecoration(
                          color: AppColors.lightGreyColor,
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(8),
                            bottomRight: Radius.circular(8),
                          ),
                        ),
                        child: Text("NEW!",
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: 14,
                                color: Colors.white)),
                      ))
                else
                  Container(),
              ]),
            ),
            AppDimens.verticalSpace8,
            Text(product.name),
            AppDimens.verticalSpace4,
            Text(
              product.time,
              style: TextStyle(fontSize: 12, color: Colors.black54),
            ),
            AppDimens.verticalSpace8,
            Row(
              children: [
                if (product.priceBeforeDiscount != "")
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text(product.priceBeforeDiscount,
                        style: TextStyle(
                            color: AppColors.lightGreyColor,
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                            decoration: TextDecoration.lineThrough)),
                  )
                else
                  Container(),
                Text(product.price,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    )),
              ],
            ),
            Row(
              children: [
                const Icon(
                  Icons.star,
                  color: Colors.yellow,
                ),
                AppDimens.horizontalSpace8,
                Text(
                  product.rating,
                  style: TextStyle(fontSize: 12, color: Colors.black54),
                ),
                AppDimens.horizontalSpace8,
                Text("|"),
                AppDimens.horizontalSpace8,
                Text(
                  product.sold,
                  style: TextStyle(fontSize: 12, color: Colors.black54),
                ),
              ],
            ),
          ]),
    );
  }
}
