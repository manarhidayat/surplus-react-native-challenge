import 'package:flutter/material.dart';
import 'package:surplus_challenge/app/core/values/app_colors.dart';
import 'package:surplus_challenge/app/core/values/app_dimens.dart';
import 'package:surplus_challenge/app/core/values/app_images.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.colorPrimary,
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: const [
                    Text(
                      "Lokasi kamu",
                      style: TextStyle(fontSize: 14, color: Colors.white),
                    ),
                    Text(
                      "Jl. Bandung - Bandung - Bandung",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ],
                ),
                const Spacer(),
                const Icon(
                  Icons.favorite,
                  color: Colors.white,
                ),
                AppDimens.horizontalSpace12,
                const Icon(Icons.shopping_basket_sharp, color: Colors.white),
              ]),
          AppDimens.verticalSpace16,
          const Text(
            "Hi, Manar Hidayat",
            style: TextStyle(
                color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
