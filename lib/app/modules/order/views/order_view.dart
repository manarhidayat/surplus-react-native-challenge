import 'package:flutter/material.dart';
import 'package:surplus_challenge/app/core/values/app_dimens.dart';
import 'package:surplus_challenge/app/core/values/app_values.dart';
import 'package:surplus_challenge/app/core/widget/custom_button.dart';

import '/app/core/base/base_view.dart';
import '/app/modules/order/controllers/order_controller.dart';

class OrderView extends BaseView<OrderController> {
  @override
  PreferredSizeWidget? appBar(BuildContext context) {
    return PreferredSize(
      preferredSize: const Size(0.0, 0.0),
      child: Container(),
    );
  }

  @override
  Widget body(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Daftar Pesanan'),
          backgroundColor: Colors.white,
          centerTitle: true,
          bottom: const TabBar(
            isScrollable: true,
            labelPadding: EdgeInsets.symmetric(horizontal: 10.0),
            tabs: [
              Tab(
                icon: Icon(Icons.payment),
                text: "Belum Dibayar",
              ),
              Tab(
                icon: Icon(Icons.shopping_basket),
                text: "Pesanan Aktif",
              ),
              Tab(
                icon: Icon(Icons.done_sharp),
                text: "Pesanan Selesai",
              ),
              Tab(
                icon: Icon(Icons.cancel_sharp),
                text: "Pesanan Dibatalkan",
              ),
            ],
          ),
        ),
        body: Stack(
          children: [
            TabBarView(
              children: [emptyCart(), emptyCart(), emptyCart(), emptyCart()],
            ),
          ],
        ),
      ),
    );
  }

  Widget emptyCart() {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.all(AppValues.padding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'Kamu belum punya pesanan nih',
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600),
          ),
          AppDimens.verticalSpace8,
          const Text(
            'Segera pesan dan selamatkan makanan favoritmu',
            style: TextStyle(color: Colors.black54),
          ),
          AppDimens.verticalSpace18,
          CustomButton(
              onPressed: () {
                print("Pesan");
              },
              text: 'Pesan Sekarang'),
        ],
      ),
    );
  }
}
