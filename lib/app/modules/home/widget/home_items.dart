// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, avoid_unnecessary_containers
import 'package:flutter/material.dart';
import 'package:surplus_challenge/app/core/model/product.dart';
import 'package:surplus_challenge/app/core/widget/item_product.dart';

class HomeItems extends StatefulWidget {
  const HomeItems({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => HomeItemsState();
}

class HomeItemsState extends State<HomeItems> {
  List<Product> productList = [
    Product(
        distance: '3.2 Km',
        status: 'Hampir habis!',
        time: 'Ambil hari ini, 08:00 - 22:00',
        rating: '5.0',
        sold: '15x terjual',
        name: "Keju",
        image:
            "https://asset.kompas.com/crops/2YUO_0WcGJTxE485xLktLFVlA10=/48x3:915x581/750x500/data/photo/2022/06/22/62b2b0c328568.jpg",
        price: "80K",
        discount: "10%",
        priceBeforeDiscount: "100K"),
    Product(
        distance: '3.2 Km',
        status: 'Hampir habis!',
        time: 'Ambil hari ini, 08:00 - 22:00',
        rating: '5.0',
        sold: '15x terjual',
        name: "Keju",
        image:
            "https://asset.kompas.com/crops/2YUO_0WcGJTxE485xLktLFVlA10=/48x3:915x581/750x500/data/photo/2022/06/22/62b2b0c328568.jpg",
        price: "80K",
        discount: "10%",
        priceBeforeDiscount: "100K"),
    Product(
        distance: '3.2 Km',
        status: 'Hampir habis!',
        time: 'Ambil hari ini, 08:00 - 22:00',
        rating: '5.0',
        sold: '15x terjual',
        name: "Keju",
        image:
            "https://asset.kompas.com/crops/2YUO_0WcGJTxE485xLktLFVlA10=/48x3:915x581/750x500/data/photo/2022/06/22/62b2b0c328568.jpg",
        price: "80K",
        discount: "10%",
        priceBeforeDiscount: "100K"),
    Product(
        distance: '3.2 Km',
        status: 'Hampir habis!',
        time: 'Ambil hari ini, 08:00 - 22:00',
        rating: '5.0',
        sold: '15x terjual',
        name: "Keju",
        image:
            "https://asset.kompas.com/crops/2YUO_0WcGJTxE485xLktLFVlA10=/48x3:915x581/750x500/data/photo/2022/06/22/62b2b0c328568.jpg",
        price: "80K",
        discount: "10%",
        priceBeforeDiscount: "100K"),
    Product(
        distance: '3.2 Km',
        status: 'Hampir habis!',
        time: 'Ambil hari ini, 08:00 - 22:00',
        rating: '5.0',
        sold: '15x terjual',
        name: "Keju",
        image:
            "https://asset.kompas.com/crops/2YUO_0WcGJTxE485xLktLFVlA10=/48x3:915x581/750x500/data/photo/2022/06/22/62b2b0c328568.jpg",
        price: "80K",
        discount: "10%",
        priceBeforeDiscount: "100K"),
    Product(
        distance: '3.2 Km',
        status: 'Hampir habis!',
        time: 'Ambil hari ini, 08:00 - 22:00',
        rating: '5.0',
        sold: '15x terjual',
        name: "Keju",
        image:
            "https://asset.kompas.com/crops/2YUO_0WcGJTxE485xLktLFVlA10=/48x3:915x581/750x500/data/photo/2022/06/22/62b2b0c328568.jpg",
        price: "80K",
        discount: "10%",
        priceBeforeDiscount: "100K"),
  ];

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: productList.length,
        itemBuilder: (BuildContext context, int index) {
          Product product = productList[index];

          return ItemProduct(product: product);
        },
      ),
    );
  }
}
