// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:surplus_challenge/app/core/base/base_view.dart';
import 'package:surplus_challenge/app/core/values/app_colors.dart';
import 'package:surplus_challenge/app/core/values/app_dimens.dart';
import 'package:surplus_challenge/app/core/values/app_images.dart';
import 'package:surplus_challenge/app/core/values/app_values.dart';
import 'package:surplus_challenge/app/core/values/text_styles.dart';
import 'package:surplus_challenge/app/modules/signin/controllers/signin_controller.dart';
import 'package:surplus_challenge/app/routes/app_pages.dart';

class SigninView extends BaseView<SigninController> {
  @override
  PreferredSizeWidget? appBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size(0.0, 0.0),
      child: Container(),
    );
  }

  @override
  Widget body(BuildContext context) {
    return GetBuilder<SigninController>(builder: (controller) {
      return Container(
        color: AppColors.colorWhite,
        height: double.infinity,
        child: Stack(children: <Widget>[
          Image.asset(
            Images.onboard,
            width: double.infinity,
          ),
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: Container(
              color: AppColors.colorWhite,
              padding: const EdgeInsets.all(16),
              child: Form(
                key: controller.formKey,
                child: SizedBox(
                  width: AppValues.widthContent,
                  child: Column(
                    children: [
                      Text("Masuk",
                          style: TextStyle(
                            fontSize: AppValues.textTitle,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          )),
                      AppDimens.verticalSpace12,
                      Text("Pastikan kamu sudah pernah membuat akun Surplus",
                          style: TextStyle(
                            color: Colors.black45,
                          )),
                      AppDimens.verticalSpace20,
                      TextFormField(
                        controller: controller.username,
                        validator: controller.checkUsername,
                        decoration: InputDecoration(
                          hintText: 'Alamat email kamu',
                          hintStyle: hintTextInput,
                          fillColor: AppColors.textFieldColor,
                          filled: true,
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 10.0),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.colorPrimary),
                            borderRadius:
                                BorderRadius.circular(AppValues.borderRadius),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.borderColor),
                            borderRadius:
                                BorderRadius.circular(AppValues.borderRadius),
                          ),
                        ),
                      ),
                      AppDimens.verticalSpace12,
                      TextFormField(
                        controller: controller.password,
                        validator: controller.checkPassword,
                        obscureText: controller.isVisible.value ? false : true,
                        enableSuggestions: false,
                        autocorrect: false,
                        decoration: InputDecoration(
                          hintText: 'Masukan kata sandi',
                          hintStyle: hintTextInput,
                          fillColor: AppColors.textFieldColor,
                          filled: true,
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 10.0),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.colorPrimary),
                            borderRadius:
                                BorderRadius.circular(AppValues.borderRadius),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.borderColor),
                            borderRadius:
                                BorderRadius.circular(AppValues.borderRadius),
                          ),
                          suffixIcon: IconButton(
                              onPressed: () {
                                controller.togglePassword();
                              },
                              iconSize: AppValues.iconSize_20,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              disabledColor: Colors.transparent,
                              focusColor: Colors.transparent,
                              icon: Icon(
                                controller.isVisible.value
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: AppColors.colorPrimary,
                              )),
                        ),
                      ),
                      AppDimens.verticalSpace12,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          GestureDetector(
                            onTap: () async {},
                            child: Text("Lupa kata sandi?",
                                style: TextStyle(
                                    color: AppColors.colorPrimary,
                                    fontWeight: FontWeight.w500)),
                          ),
                        ],
                      ),
                      AppDimens.verticalSpace12,
                      SizedBox(
                        width: double.infinity,
                        height: AppValues.heightButton,
                        child: OutlinedButton(
                          style: ButtonStyle(
                            padding: MaterialStateProperty.resolveWith(
                                (states) => const EdgeInsets.all(8.0)),
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => AppColors.colorPrimary),
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        AppValues.borderRadius))),
                          ),
                          onPressed: () {
                            controller.login();
                          },
                          child: Text(
                            'Masuk',
                            style: textButtonWhite,
                          ),
                        ),
                      ),
                      AppDimens.verticalSpace12,
                      Row(children: <Widget>[
                        Expanded(
                            child: Divider(
                          thickness: 1,
                          color: AppColors.borderColor,
                        )),
                        Padding(
                          padding: EdgeInsets.all(AppValues.halfPadding),
                          child: Text(
                            "Atau",
                            style: TextStyle(color: AppColors.textColorPrimary),
                          ),
                        ),
                        Expanded(
                            child: Divider(
                          thickness: 1,
                          color: AppColors.borderColor,
                        )),
                      ]),
                      AppDimens.verticalSpace12,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          SizedBox(
                            width: 150,
                            height: AppValues.heightButton,
                            child: OutlinedButton(
                                style: OutlinedButton.styleFrom(
                                  backgroundColor: AppColors.textFieldColor,
                                  side:
                                      BorderSide(color: AppColors.colorPrimary),
                                  shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(
                                              AppValues.borderRadius))),
                                ),
                                onPressed: () async {},
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      // Image.asset(
                                      //   Images.ic_google,
                                      //   width: AppValues.iconDefaultSize,
                                      // ),
                                      AppDimens.horizontalSpace4,
                                      Text(
                                        'Facebook',
                                        style: TextStyle(
                                            color: AppColors.colorPrimary,
                                            fontSize: AppValues.fontSize_14,
                                            fontWeight: FontWeight.w500),
                                      )
                                    ])),
                          ),
                          SizedBox(
                            width: 150,
                            height: AppValues.heightButton,
                            child: OutlinedButton(
                                style: OutlinedButton.styleFrom(
                                  backgroundColor: AppColors.textFieldColor,
                                  side:
                                      BorderSide(color: AppColors.colorPrimary),
                                  shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(
                                              AppValues.borderRadius))),
                                ),
                                onPressed: () async {},
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      // Image.asset(
                                      //   Images.ic_google,
                                      //   width: AppValues.iconDefaultSize,
                                      // ),
                                      AppDimens.horizontalSpace4,
                                      Text(
                                        'Google',
                                        style: TextStyle(
                                            color: AppColors.colorPrimary,
                                            fontSize: AppValues.fontSize_14,
                                            fontWeight: FontWeight.w500),
                                      )
                                    ])),
                          ),
                        ],
                      ),
                      AppDimens.verticalSpace20,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Belum punya akun? ",
                              style:
                                  TextStyle(color: AppColors.textColorPrimary)),
                          GestureDetector(
                            onTap: () async {
                              Get.toNamed(Routes.SIGNUP);
                            },
                            child: Text("Yuk daftar",
                                style:
                                    TextStyle(color: AppColors.colorPrimary)),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ]),
      );
    });
  }
}
