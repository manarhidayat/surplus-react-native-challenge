// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:surplus_challenge/app/core/values/app_colors.dart';
import 'package:surplus_challenge/app/core/values/app_dimens.dart';
import 'package:surplus_challenge/app/core/values/app_images.dart';
import 'package:surplus_challenge/app/core/values/app_values.dart';
import 'package:surplus_challenge/app/core/values/text_styles.dart';
import 'package:surplus_challenge/app/modules/signup/controllers/signup_controller.dart';
import 'package:surplus_challenge/app/routes/app_pages.dart';
import '/app/core/base/base_view.dart';
import '/app/core/widget/custom_app_bar.dart';

class SignupView extends BaseView<SignupController> {
  @override
  PreferredSizeWidget? appBar(BuildContext context) {
    return PreferredSize(
      preferredSize: Size(0.0, 0.0),
      child: Container(),
    );
  }

  @override
  Widget body(BuildContext context) {
    return GetBuilder<SignupController>(builder: (controller) {
      return Container(
        color: AppColors.colorWhite,
        height: double.infinity,
        child: Stack(children: <Widget>[
          Image.asset(
            Images.onboard,
            width: double.infinity,
          ),
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: Container(
              color: AppColors.colorWhite,
              padding: const EdgeInsets.all(16),
              child: Form(
                key: controller.formKey,
                child: SizedBox(
                  width: AppValues.widthContent,
                  child: Column(
                    children: [
                      Text("Daftar",
                          style: TextStyle(
                            fontSize: AppValues.textTitle,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          )),
                      AppDimens.verticalSpace12,
                      Text("Pastikan kamu sudah pernah membuat akun Surplus",
                          style: TextStyle(
                            color: Colors.black45,
                          )),
                      AppDimens.verticalSpace20,
                      TextFormField(
                        controller: controller.username,
                        validator: controller.checkUsername,
                        decoration: InputDecoration(
                          hintText: 'Alamat email kamu',
                          hintStyle: hintTextInput,
                          fillColor: AppColors.textFieldColor,
                          filled: true,
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 10.0),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.colorPrimary),
                            borderRadius:
                                BorderRadius.circular(AppValues.borderRadius),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.borderColor),
                            borderRadius:
                                BorderRadius.circular(AppValues.borderRadius),
                          ),
                        ),
                      ),
                      AppDimens.verticalSpace12,
                      TextFormField(
                        controller: controller.password,
                        validator: controller.checkPassword,
                        obscureText: controller.isVisible.value ? false : true,
                        enableSuggestions: false,
                        autocorrect: false,
                        decoration: InputDecoration(
                          hintText: 'Masukan kata sandi',
                          hintStyle: hintTextInput,
                          fillColor: AppColors.textFieldColor,
                          filled: true,
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 10.0),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.colorPrimary),
                            borderRadius:
                                BorderRadius.circular(AppValues.borderRadius),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: AppColors.borderColor),
                            borderRadius:
                                BorderRadius.circular(AppValues.borderRadius),
                          ),
                          suffixIcon: IconButton(
                              onPressed: () {
                                controller.togglePassword();
                              },
                              iconSize: AppValues.iconSize_20,
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              hoverColor: Colors.transparent,
                              disabledColor: Colors.transparent,
                              focusColor: Colors.transparent,
                              icon: Icon(
                                controller.isVisible.value
                                    ? Icons.visibility_off
                                    : Icons.visibility,
                                color: AppColors.colorPrimary,
                              )),
                        ),
                      ),
                      AppDimens.verticalSpace12,
                      SizedBox(
                        width: double.infinity,
                        height: AppValues.heightButton,
                        child: OutlinedButton(
                          style: ButtonStyle(
                            padding: MaterialStateProperty.resolveWith(
                                (states) => const EdgeInsets.all(8.0)),
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => AppColors.colorPrimary),
                            shape: MaterialStateProperty.all(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(
                                        AppValues.borderRadius))),
                          ),
                          onPressed: () {
                            controller.login();
                          },
                          child: Text(
                            'Daftar',
                            style: textButtonWhite,
                          ),
                        ),
                      ),
                      AppDimens.verticalSpace20,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Sudah punya akun? ",
                              style:
                                  TextStyle(color: AppColors.textColorPrimary)),
                          GestureDetector(
                            onTap: () async {
                              Get.toNamed(Routes.SIGNIN);
                            },
                            child: Text("masuk",
                                style:
                                    TextStyle(color: AppColors.colorPrimary)),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ]),
      );
    });
  }
}
