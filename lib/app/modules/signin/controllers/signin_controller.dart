import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:surplus_challenge/app/routes/app_pages.dart';

import '/app/core/base/base_controller.dart';

class SigninController extends BaseController {
  // final box = GetStorage(AppConstanst.storageName);

  final formKey = GlobalKey<FormState>();
  final username = TextEditingController();
  final password = TextEditingController();
  final isVisible = false.obs;

  // final AuthRepository _repository = Get.find(tag: (AuthRepository).toString());

  void togglePassword() {
    isVisible(!isVisible.value);
    update();
  }

  @override
  void onClose() {
    username.dispose();
    password.dispose();
    super.onClose();
  }

  String? checkUsername(String? username) {
    if (username != null) {
      if (username.isEmpty || username.length < 8) {
        return 'Email min 8 characters';
      }
    }

    return null;
  }

  String? checkPassword(String? password) {
    if (password != null) {
      if (password.isEmpty || password.length < 8) {
        return 'Password min 8 characters';
      }
    }

    return null;
  }

  Future<void> login() async {
    if (!formKey.currentState!.validate()) {
      return;
    }

    Get.toNamed(Routes.MAIN);
  }

  Future<void> loginGoogle(String token) async {
    
  }

}
