import 'package:flutter/material.dart';

import '/app/modules/main/model/menu_code.dart';

class BottomNavItem {
  final String navTitle;
  final IconData icon;
  final MenuCode menuCode;

  const BottomNavItem(
      {required this.navTitle,
      required this.icon,
      required this.menuCode});
}
